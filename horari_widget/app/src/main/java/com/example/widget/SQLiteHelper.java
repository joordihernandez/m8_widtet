package com.example.widget;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.preference.PreferenceManager;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

public class SQLiteHelper extends SQLiteOpenHelper {

    private String columnas = "ID_HORARI, NOM_GRUP, NOM_MODUL, NOM_PROFESSOR, NOM_AULA, HORA_INICI, HORA_FI, DIA_SETMANA";
    private String tablas = "HORARIS H, GRUPS G, MODULS M, PROFESSORS P, AULAS A";
    private String join = "H.GRUP = G.ID_GRUP AND H.MODUL = M.ID_MODUL AND H.PROFESSOR = P.ID_PROFESSOR AND H.AULA = A.ID_AULA";
    private Context context;

    public SQLiteHelper(Context context) {
        super(context, "DB", null, 1);
        this.context = context;
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        String[] sql = {"CREATE TABLE PROFESSORS (ID_PROFESSOR INTEGER PRIMARY KEY AUTOINCREMENT, NOM_PROFESSOR TEXT NOT NULL)",
                "CREATE TABLE MODULS (ID_MODUL INTEGER PRIMARY KEY AUTOINCREMENT, NOM_MODUL TEXT NOT NULL)",
                "CREATE TABLE AULAS (ID_AULA INTEGER PRIMARY KEY AUTOINCREMENT, NOM_AULA TEXT NOT NULL)",
                "CREATE TABLE GRUPS (ID_GRUP INTEGER PRIMARY KEY AUTOINCREMENT, NOM_GRUP TEXT NOT NULL)",
                "CREATE TABLE HORARIS (ID_HORARI INTEGER PRIMARY KEY AUTOINCREMENT, GRUP INTEGER, MODUL INTEGER, PROFESSOR INTEGER, " + "AULA INTEGER, HORA_INICI TEXT NOT NULL, HORA_FI TEXT NOT NULL, DIA_SETMANA INTEGER NOT NULL)",
                "INSERT INTO PROFESSORS VALUES (null, 'Raül Vallez'), (null, 'Josefa Gonzàlez'), (null, 'Jose Leo'), (null, 'Lluís Perpiñà'), (null, 'Marta Planas'),(null, 'Andrés Prieto')",
                "INSERT INTO MODULS VALUES (null, 'M06'), (null, 'M03'), (null, 'M07'), (null, 'M08'), (null, 'M09'), (null, 'M10'), (null, 'TUT'),(null, 'DUAL')",
                "INSERT INTO AULAS VALUES (null, '217'), (null, '201')",
                "INSERT INTO GRUPS VALUES (null, '2DAM-A'), (null, '2DAM-B')",
                "INSERT INTO HORARIS VALUES " +
                        "(null, 1, 8, 6, 1, '15:00:00', '15:59:59', 1)," +
                        "(null, 2, 8, 6, 2, '15:00:00', '15:59:59', 1), " +
                        "(null, 1, 6, 6, 1, '16:00:00', '17:59:59', 1)," +
                        "(null, 2, 4, 6, 2, '16:00:00', '17:59:59', 1), " +
                        "(null, 1, 1, 2, 1, '18:20:00', '20:19:59', 1)," +
                        "(null, 2, 6, 2, 2, '18:20:00', '19:19:59', 1), " +
                        "(null, 1, 8, 3, 1, '15:00:00', '16:59:59', 2)," +
                        "(null, 2, 8, 3, 2, '15:00:00', '16:59:59', 2), " +
                        "(null, 1, 4, 4, 1, '15:00:00', '16:59:59', 2)," +
                        "(null, 2, 4, 4, 2, '17:00:00', '17:59:59', 2)," +
                        "(null, 1, 4, 4, 1, '18:20:00', '19:19:59', 2)," +
                        "(null, 2, 5, 5, 2, '18:20:00', '19:19:59', 2)," +
                        "(null, 1, 5, 5, 1, '19:20:00', '21:19:59', 2)," +
                        "(null, 2, 4, 4, 2, '19:20:00', '20:19:59', 2),"+
                        "(null, 1, 8, 6, 1, '16:00:00', '17:59:59', 3)," +
                        "(null, 2, 8, 6, 2, '16:00:00', '17:59:59', 3),"+
                        "(null, 1, 7, 2, 1, '18:20:00', '19:19:59', 3)," +
                        "(null, 2, 7, 2, 2, '18:20:00', '19:19:59', 3),"+
                        "(null, 1, 2, 2, 1, '19:20:00', '20:19:59', 3)," +
                        "(null, 2, 4, 4, 2, '19:20:00', '20:19:59', 3),"+
                        "(null, 1, 4, 4, 1, '20:20:00', '21:19:59', 3),"+
                        "(null, 2, 2, 2, 2, '20:20:00', '21:19:59', 3),"+
                        "(null, 2, 8, 3, 2, '15:00:00', '16:59:59', 4)," +
                        "(null, 1, 8, 3, 1, '15:00:00', '16:59:59', 4),"+
                        "(null, 1, 2, 2, 1, '17:00:00', '18:59:59', 4),"+
                        "(null, 2, 3, 1, 2, '17:00:00', '18:59:59', 4),"+
                        "(null, 1, 2, 2, 1, '18:20:00', '19:19:59', 4),"+
                        "(null, 2, 3, 1, 2, '18:20:00', '19:19:59', 4),"+
                        "(null, 2, 2, 2, 1, '19:20:00', '21:19:59', 4),"+
                        "(null, 1, 3, 1, 2, '19:20:00', '21:19:59', 4),"+
                        "(null, 1, 4, 4, 1, '15:00:00', '16:59:59', 5),"+
                        "(null, 2, 2, 2, 2, '15:00:00', '16:59:59', 5),"+
                        "(null, 1, 2, 2, 1, '17:00:00', '17:59:59', 5),"+
                        "(null, 2, 1, 1, 2, '18:20:00', '20:19:59', 5),"+
                        "(null, 1, 2, 2, 1, '18:20:00', '19:19:59', 5)"};
        for (String sentencia : sql) {
            sqLiteDatabase.execSQL(sentencia);
        }
    }

    public ArrayList<Horario> getHorarioDia(String grupo) {
        Calendar calendar = Calendar.getInstance();
        int dayOfWeek = calendar.get(Calendar.DAY_OF_WEEK) -1;
        Cursor c = this.getReadableDatabase().rawQuery("SELECT " + columnas + " FROM " + tablas +
                " WHERE " + join + " AND DIA_SETMANA = ? AND NOM_GRUP = ?", new String[]{String.valueOf(dayOfWeek), grupo});
        return getHorarioCursor(c);
    }

    public String getHorarioHoras(String grupo) {
        Calendar cal = Calendar.getInstance();
        cal.setFirstDayOfWeek(Calendar.MONDAY);
        SimpleDateFormat fermatter = new SimpleDateFormat("HH:mm:ss");
        String grup = PreferenceManager.getDefaultSharedPreferences(context).getString(context.getString(R.string.grupo), ""+grupo);
        String diaSemana = String.valueOf(getDaysOfWeek()[cal.get(Calendar.DAY_OF_WEEK) - 1]);
        Cursor cur = this.getReadableDatabase().rawQuery("SELECT " + columnas + " FROM " + tablas +
                        " WHERE " + join + " AND DIA_SETMANA = ? AND NOM_GRUP = ? AND ? BETWEEN HORA_INICI AND HORA_FI",
                new String[]{diaSemana, grup, fermatter.format(cal.getTime())});
        ArrayList<Horario> horarios = getHorarioCursor(cur);
        return (horarios.toString());
    }

    private int[] getDaysOfWeek() {
        return new int[]{7, 1, 2, 3, 4, 5, 6};
    }

    private ArrayList<Horario> getHorarioCursor(Cursor cur) {
        ArrayList<Horario> horarios = new ArrayList<>();
        if (cur.moveToFirst()) {
            do {
                horarios.add(new Horario(cur.getInt(0), cur.getString(1), cur.getString(2)
                        , cur.getString(3), cur.getString(4), cur.getString(5), cur.getString(6), cur.getInt(7)));
            } while (cur.moveToNext());
        }
        cur.close();
        return horarios;
    }

    @Override
    public String toString() {
        return "SQLiteHelper{" +
                "columnas='" + columnas + '\'' +
                ", tablas='" + tablas + '\'' +
                ", join='" + join + '\'' +
                ", context=" + context +
                '}';
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int versionAnterior, int versionNueva) {

    }
}
