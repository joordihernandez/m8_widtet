package com.example.widget;

public class Horario {

    private int id,dia_semana;
    private String grupo,modulo,hora_inicio,hora_final,professor,classe;

    public Horario(int id, String grupo, String modulo, String professor, String classe, String hora_inicio, String hora_final, int dia_semana) {
        this.id = id;
        this.grupo = grupo;
        this.modulo = modulo;
        this.professor = professor;
        this.classe = classe;
        this.hora_inicio = hora_inicio;
        this.hora_final = hora_final;
        this.dia_semana = dia_semana;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getGrupo() {
        return grupo;
    }

    public void setGrupo(String grupo) {
        this.grupo = grupo;
    }

    public String getModulo() {
        return modulo;
    }

    public void setModulo(String modulo) {
        this.modulo = modulo;
    }

    public String getProfessor() {
        return professor;
    }

    public void setProfessor(String professor) {
        this.professor = professor;
    }

    public String getClasse() {
        return classe;
    }

    public void setClasse(String classe) {
        this.classe = classe;
    }

    public String getHora_inicio() {
        return hora_inicio;
    }

    public void setHora_inicio(String hora_inicio) {
        this.hora_inicio = hora_inicio;
    }

    public String getHora_final() {
        return hora_final;
    }

    public void setHora_final(String hora_final) {
        this.hora_final = hora_final;
    }

    public int getDia_semana() {
        return dia_semana;
    }

    public void setDia_semana(int dia_semana) {
        this.dia_semana = dia_semana;
    }

    @Override
    public String toString() {
        return
                "\n Grup " + grupo + "\t"+
                "Modul " + modulo +"\t"+
                "Professor " + professor+"\t"+
                "Aula " + classe + "\t"+
                "Inici " + hora_inicio +"\t"+
                "Fins " + hora_final + "\t"+
                "Dia" + dia_semana +"\n" ;
    }
}